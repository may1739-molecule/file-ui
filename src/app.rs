#![feature(plugin)]
#![plugin(rocket_codegen)]

use std::io::{self, Read};
use std::path::{Path, PathBuf};

extern crate common_actions;
extern crate futures;
extern crate molecule_common;
extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate tokio_core;
extern crate uuid;

use common_actions::{FILE_ACTION, FileActionData};
use futures::Future;
use futures::stream::Wait;
use futures::Stream;
use molecule_common::app;
use molecule_common::reqes::Reqes;
use molecule_common::message::*;
use rocket::response::{self, NamedFile};
use rocket_contrib::Template;
use rocket::State;
use tokio_core::reactor::Core;
use uuid::Uuid;

#[derive(Copy, Clone, Eq, PartialEq, Serialize)]
enum FileType {
    Audio,
    Video,
    Other,
}

#[derive(Serialize)]
struct FileItem {
    video: bool,
    audio: bool,
    other: bool,
    name: PathBuf,
}

struct DataStreamWrapper {
    buffer: Vec<u8>,
    inner: Wait<DataStream>,
}

impl From<DataStream> for DataStreamWrapper {
    fn from(src: DataStream) -> DataStreamWrapper {
        DataStreamWrapper {
            buffer: Vec::new(),
            inner: src.wait(),
        }
    }
}

impl Read for DataStreamWrapper {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let buf_len = buf.len();

        if !self.buffer.is_empty() {
            if self.buffer.len() > buf_len {
                let drain = self.buffer.drain(..buf_len).collect::<Vec<_>>();
                buf.copy_from_slice(&drain);

                return Ok(buf_len);
            } else {
                let len = self.buffer.len();
                buf[..len].copy_from_slice(&self.buffer);
                self.buffer.clear();
                return Ok(len);
            }
        }

        match self.inner.next() {
            Some(Ok(data_buf)) => {
                if data_buf.len() > buf_len {
                    buf.copy_from_slice(&data_buf[..buf_len]);
                    self.buffer.clear();
                    self.buffer.extend_from_slice(&data_buf[buf_len..]);

                    Ok(buf_len)
                } else {
                    buf[..data_buf.len()].copy_from_slice(&data_buf);

                    Ok(data_buf.len())
                }
            }
            Some(Err(err)) => Err(err),
            None => Ok(0),
        }
    }
}

#[get("/video/<file..>")]
fn get_video(file: PathBuf) -> Template {
    Template::render("video", &file)
}

#[get("/audio/<file..>")]
fn get_audio(file: PathBuf) -> Template {
    Template::render("audio", &file)
}

#[get("/file/<file..>")]
fn get_file(file: PathBuf,
            reqes: State<Reqes<Message>>)
            -> std::io::Result<response::Stream<DataStreamWrapper>> {
    let destination = Signature::new(RoutingType::Any, RoutingType::One(Uuid::nil())); // TODO get the particles actual uuid
    let source = Signature::new(RoutingType::One(Uuid::nil()), RoutingType::One(Uuid::nil())); // TODO get our actual uuid
    let message = MessageBuilder::new(FILE_ACTION)
        .destination(destination)
        .source(source)
        .trace("file-ui", Uuid::nil())
        .data(&FileActionData::Get(file.to_string_lossy().into_owned()))?
        .build_packet();

    let mut res = reqes.client
        .send(message)
        .wait()?;
    let data_reader: DataStreamWrapper = res.get_stream().unwrap().into();


    Ok(response::Stream::from(data_reader))
}

#[get("/file")]
fn get_files(reqes: State<Reqes<Message>>) -> std::io::Result<Template> {
    let destination = Signature::new(RoutingType::Any, RoutingType::One(Uuid::nil())); // TODO get the particles actual uuid
    let source = Signature::new(RoutingType::One(Uuid::nil()), RoutingType::One(Uuid::nil())); // TODO get our actual uuid
    let message = MessageBuilder::new(FILE_ACTION)
        .destination(destination)
        .source(source)
        .trace("file-ui", Uuid::nil())
        .data(&FileActionData::List)?
        .build_packet();

    let res = reqes.client
        .send(message)
        .wait()?;

    let files = res.get_data::<Vec<String>>()?
        .iter()
        .map(Path::new)
        .filter_map(|path| {
            path.extension().map(|ext| {
                let file_type = match ext.to_str() {
                    Some("mp4") => FileType::Video,
                    Some("mp3") => FileType::Audio,
                    _ => FileType::Other,

                };

                FileItem {
                    video: file_type == FileType::Video,
                    audio: file_type == FileType::Audio,
                    other: file_type == FileType::Other,
                    name: PathBuf::from(path),
                }
            })
        })
        .collect::<Vec<_>>();

    Ok(Template::render("list", &files))
}

#[get("/")]
fn index() -> io::Result<NamedFile> {
    NamedFile::open("assets/index.html")
}

fn main() {
    let mut core = Core::new().unwrap();
    let reqes = app::start(core.handle()).unwrap();

    std::thread::spawn(|| {
        rocket::ignite()
            .mount("/",
                   routes![index, get_file, get_files, get_audio, get_video])
            .manage(reqes)
            .launch()
    });

    loop {
        core.turn(None);
    }
}